const fs = require('fs');

function readInput(path) {
    return fs.readFileSync(path).toString();
}

text = readInput('c:\\Users\\Rin\\Programming\\React -- JavaScript\\adventskalender_2020\\Day06\\day06b.txt')

groups = text.split("\r\n\r\n")

letters = 0
for (group of groups) {
    set = {}
    lines = group.split("\r\n")
    for (line of lines) {
        for (letter of line) {
            if (!set[letter]) {
                set[letter] = 0
            }
            set[letter] = set[letter] + 1
        }
    }
    for (letter in set) {
        if (set[letter] === lines.length) {
            letters ++
        }
    }
}
console.log(letters)