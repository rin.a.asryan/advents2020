const fs = require('fs');

function readInput(path) {
    return fs.readFileSync(path).toString().split('\n');
}

text = readInput('c:\\Users\\Rin\\Programming\\React -- JavaScript\\adventskalender_2020\\Day03\\day03b.txt')

function calc (dX, dY, lines) {
    trees = 0
    x = 0
    for (i = 0; i < lines.length - dY; i += dY ) {
        x += dX
        size = lines[0].replace(/(\r|\n)/gm,"").length
        if (x >= size) {
            x -= size
        }
        if (lines[i + dY][x] === "#") {
            trees ++
        }
//        console.log(i, lines[i + dY][x])
    }
    return trees
}

console.log(calc(1, 1, text) * calc(3, 1, text) * calc(5, 1, text) * calc(7, 1, text) * calc(1, 2, text))