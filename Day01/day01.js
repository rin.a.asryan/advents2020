const fs = require('fs');

function readInput(path) {
    return fs.readFileSync(path).toString().split('\n');
}

text = readInput('c:\\Users\\Rin\\Programming\\React -- JavaScript\\adventskalender_2020\\Day01\\day01b.txt')

for (i in text) {
    a = parseInt(text[i])
    for (j in text.slice(i)) {
        b = parseInt(text[j])
        if (a + b === 2020) {
            console.log(a * b)
        }
    }
}

for (i in text) {
    a = parseInt(text[i])
    for (j in text) {
        b = parseInt(text[j])
        for (k in text) {
            c = parseInt(text[k])
            if (a + b + c === 2020) {
                console.log(a * b * c)
            }
        }
    }
}