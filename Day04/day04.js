const fs = require('fs');
function readInput(path) {
    return fs.readFileSync(path).toString().split('\r\n\r\n');
}
text = readInput('c:\\Users\\Rin\\Programming\\React -- JavaScript\\adventskalender_2020\\Day04\\day04b.txt')
valid = 0
passports:
for (pp of text) {
    parts = pp.replace(/\r\n/gm," ").split(" ")
    valid ++
    if (parts.length < 7 || parts.length > 8) {
        valid --
        continue
    }
    passport = {}
    for (part of parts) {
        key = part.split(":")[0]
        if (passport[key] !== undefined) {
            valid --
            continue passports
        }
        value = part.split(":")[1]
        passport[key] = value
    }
    if (isValid(passport) === false) {
        valid --
    }
}

function isValid(passport) {
    byr = passport["byr"]
    if (byr === undefined) {
        return false
    }
    if (byr > 2002 || byr < 1920) {
        return false
    }

    iyr = passport["iyr"]
    if (iyr === undefined) {
        return false
    }
    if (iyr > 2020 || iyr < 2010) {
        return false
    }

    eyr = passport["eyr"]
    if (eyr === undefined) {
        return false
    }
    if (eyr > 2030 || eyr < 2020) {
        return false
    }

    hgt = passport["hgt"]
    if (hgt === undefined) {
        return false
    }
    if (hgt.slice(-2) === "in") {
        if (hgt.slice(0, -2 ) > 76 || hgt.slice(0, -2) < 59) {
            return false
        }
    } else if (hgt.slice(-2) === "cm") {
        if (hgt.slice(0, -2 ) > 193 || hgt.slice(0, -2) < 150) {
            return false
        }
        
    } else return false

    ecl = passport["ecl"]
    if (ecl === undefined) {
        return false
    }
    if (ecl !== "amb" && ecl !== "blu" && ecl !== "brn" && ecl !== "gry" && ecl !== "grn" && ecl !== "hzl" && ecl !== "oth") {
        return false
    }

    hcl = passport["hcl"]
    if (hcl === undefined) {
        return false
    }
    if (hcl.length !== 7) {
        return false
    }
    if (hcl[0] !== "#") {
        return false
    }
    re = /[0-9a-f]{6}/g
    if (!re.test(hcl.slice(-6))) {
        return false
    }

    pid = passport["pid"]
    if (pid === undefined) {
        return false
    }
    re = /[0-9]{9}/g
    return re.test(pid) 
}
console.log(valid)