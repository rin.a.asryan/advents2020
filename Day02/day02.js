const fs = require('fs');

function readInput(path) {
    return fs.readFileSync(path).toString().split('\n');
}

text = readInput('c:\\Users\\Rin\\Programming\\React -- JavaScript\\adventskalender_2020\\Day02\\day02b.txt')

works = 0
for (line of text) {
    parts = line.split(": ")
    password = parts[1]
    left = parts[0].split(" ")
    letter = left[1]
    minmax = left[0].split("-")
    min = parseInt(minmax[0])
    max = parseInt(minmax[1])

    count = 0
    for (c of password) {
        if (c === letter) {
            count ++
        }
    }
    if (count >= min && count <= max) {works ++}
}
console.log(works)

works = 0
for (line of text) {
    parts = line.split(": ")
    password = parts[1]
    left = parts[0].split(" ")
    letter = left[1]
    minmax = left[0].split("-")
    min = parseInt(minmax[0])
    max = parseInt(minmax[1])

    first = password[min - 1] === letter
    second = password[max - 1] === letter
    if (first !== second) {works ++}
}
console.log(works)